#!/bin/bash

. ../01-setup/00_set_params.sh

# EUROPE
gcloud beta container \
	--project "$projectID" \
	clusters create "$k8s_cluster_eu_name"  \
	--zone ${k8s_cluster_eu_zone}       \
	--username "admin"           \
	--cluster-version "1.8.10-gke.0" \
	--machine-type "n1-standard-1"   \
	--image-type "COS"               \
	--disk-type "pd-standard" --disk-size "100" \
	--scopes "https://www.googleapis.com/auth/compute","https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
	--num-nodes "1" \
	--enable-cloud-logging \
	--enable-cloud-monitoring \
	--network "default" --subnetwork "default" \
	--enable-autoscaling --min-nodes "1" \
	--max-nodes "10" \
	--addons HorizontalPodAutoscaling,HttpLoadBalancing,KubernetesDashboard \
	--enable-autorepair


# AMERICA
gcloud beta container \
	--project "$projectID" \
	clusters create "$k8s_cluster_us_name"  \
	--zone ${k8s_cluster_us_zone}       \
	--username "admin"           \
	--cluster-version "1.8.10-gke.0" \
	--machine-type "n1-standard-1"   \
	--image-type "COS"               \
	--disk-type "pd-standard" --disk-size "100" \
	--scopes "https://www.googleapis.com/auth/compute","https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
	--num-nodes "1" \
	--enable-cloud-logging \
	--enable-cloud-monitoring \
	--network "default" --subnetwork "default" \
	--enable-autoscaling --min-nodes "1" \
	--max-nodes "10" \
	--addons HorizontalPodAutoscaling,HttpLoadBalancing,KubernetesDashboard \
	--enable-autorepair


# ASIA
gcloud beta container \
	--project "$projectID" \
	clusters create "$k8s_cluster_asia_name"  \
	--zone ${k8s_cluster_asia_zone}       \
	--username "admin"           \
	--cluster-version "1.8.10-gke.0" \
	--machine-type "n1-standard-1"   \
	--image-type "COS"               \
	--disk-type "pd-standard" --disk-size "100" \
	--scopes "https://www.googleapis.com/auth/compute","https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
	--num-nodes "1" \
	--enable-cloud-logging \
	--enable-cloud-monitoring \
	--network "default" --subnetwork "default" \
	--enable-autoscaling --min-nodes "1" \
	--max-nodes "10" \
	--addons HorizontalPodAutoscaling,HttpLoadBalancing,KubernetesDashboard \
	--enable-autorepair


