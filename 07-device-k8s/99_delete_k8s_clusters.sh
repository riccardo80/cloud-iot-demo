#!/bin/bash

. ../01-setup/00_set_params.sh

# EUROPE
gcloud container \
	clusters delete "$k8s_cluster_eu_name"  \
	--zone ${k8s_cluster_eu_zone}           \
	--async

# US
gcloud container \
	clusters delete "$k8s_cluster_us_name"  \
	--zone ${k8s_cluster_us_zone}           \
	--async
# ASIA
gcloud container \
	clusters delete "$k8s_cluster_asia_name"  \
	--zone ${k8s_cluster_asia_zone}           \
	--async
